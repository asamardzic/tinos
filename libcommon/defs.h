/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DEFS_H
#define DEFS_H

/* common typedefs */
typedef char    Bool;
typedef unsigned char Byte;
typedef unsigned short Word;
typedef unsigned long DWord;

/* common constants */
#ifndef NULL
#define NULL        0		/* NULL pointer */
#endif

#ifndef TRUE
#define TRUE        1		/* TRUE value */
#endif

#ifndef FALSE
#define FALSE       0		/* FALSE value */
#endif

#ifndef PI
#define PI           3.14159265358979323846	/* PI number */
#endif

#ifndef DBL_MAX
#define DBL_MAX      1.7976931348623157E+308	/* Maximum double value */
#endif

/* useful indices */
#define LO          0
#define HI          1

/* useful macros */
#define FOREVER     for ( ; ; )
#define Max(A,B)    ((A>B) ? A : B)
#define Min(A,B)    ((A<B) ? A : B)
#define Swap(A,B)   (A)^=(B), (B)^=(A), (A)^=(B);
#define Signum(A)   ((A>0) ? 1 : ((A==0) ? 0 : -1))

#endif
