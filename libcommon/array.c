/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>		/* for assert() function */
#include <stdlib.h>		/* for malloc() function */
#include <string.h>		/* for memcpy() function */
#include "array.h"

/* Array_Init - Array class constructor */
void
Array_Init(this, maxItems, resizeItems)
     PArray         *this;
     int             maxItems;
     int             resizeItems;
{
	this->numItems = 0;

	/* allocate storage for maxItems items */
	this->maxItems = maxItems;
	this->ppItems = malloc(this->maxItems * sizeof(void *));
	assert(this->ppItems != NULL);

	this->resizeItems = resizeItems;
}

/* Array_Clean - Array class destructor */
void
Array_Clean(this)
     PArray         *this;
{
	free(this->ppItems);
}

/* Array_AddItem - add pointer to the item to the end of array */
int
Array_AddItem(this, pItem)
     PArray         *this;
     void           *pItem;
{
	void          **ppItems;

	if (this->numItems == this->maxItems) {	/* array storage needs to
						 * be extended */
		if (this->resizeItems == 0)	/* array is not resizeable 
						 */
			return 0;

		/* extend storage for resizeItems items */
		ppItems =
		    malloc((this->maxItems +
			    this->resizeItems) * sizeof(void *));
		assert(ppItems != NULL);
		memcpy(ppItems, this->ppItems,
		       this->maxItems * sizeof(void *));
		free(this->ppItems);
		this->ppItems = ppItems;
		this->maxItems += this->resizeItems;
	}

	/* store item */
	this->ppItems[(this->numItems)++] = pItem;

	return 1;
}

/* Array_GetItem - return pointer to the item at index specified */
void           *
Array_GetItem(pArray, index)
     PArray         *pArray;
     int             index;
{
	return pArray->ppItems[index];
}

/* Array_SetItem - set pointer to the item at index specified */
void
Array_SetItem(pArray, index, pItem)
     PArray         *pArray;
     int             index;
     void           *pItem;
{
	pArray->ppItems[index] = pItem;
}

/* ArrayIterator_Attach - attach array to iterator and initialize iterator 
 */
void
ArrayIterator_Attach(this, pArray)
     PArrayIterator *this;
     PArray         *pArray;
{
	this->pArray = pArray;
	this->currItem = 0;
}

/* ArrayIterator_Next - return pointer to next item from array */
int
ArrayIterator_Next(this, ppItem)
     PArrayIterator *this;
     void          **ppItem;
{
	PArray         *pArray = this->pArray;

	if (this->currItem >= pArray->numItems)	/* end of iteration */
		return 0;

	*ppItem = pArray->ppItems[(this->currItem)++];
	return 1;
}
