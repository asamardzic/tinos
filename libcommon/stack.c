/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>		/* for assert() function */
#include <stdlib.h>		/* for malloc() function */
#include "stack.h"

/* Stack_Init - Stack class constructor */
void
Stack_Init(this)
     PStack         *this;
{
	this->pTop = NULL;
}

/* Stack_Clean - Stack class destructor */
void
Stack_Clean(this)
     PStack         *this;
{
	PListNode      *pTop,
	               *pCurr;

	for (pTop = this->pTop; pTop;) {
		pCurr = pTop;
		pTop = pTop->pNext;
		free(pCurr);
	}
}

/* Stack_Push - insert item at the top of stack */
void
Stack_Push(this, pItem)
     PStack         *this;
     void           *pItem;
{
	PListNode      *pNew;

	pNew = malloc(sizeof(PListNode));
	assert(pNew != NULL);
	pNew->pItem = pItem;
	pNew->pNext = this->pTop;
	this->pTop = pNew;
}

/* Stack_Pop - remove item from the top of stack */
void           *
Stack_Pop(this)
     PStack         *this;
{
	PListNode      *pTop;
	void           *pItem;

	if ((pTop = this->pTop) == NULL)
		return NULL;

	this->pTop = this->pTop->pNext;
	pItem = pTop->pItem;
	free(pTop);
	return pItem;
}

/* Stack_IsEmpty - check if there are items in stack */
int
Stack_IsEmpty(this)
     PStack         *this;
{
	return this->pTop == NULL;
}
