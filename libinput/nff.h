/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NFF_H
#define NFF_H

/* helper classes not mentioned by NFF format */

/* NFFPoint class */
typedef struct _NFFPoint {
	float           x,
	                y,
	                z;
} NFFPoint;

/* NFFColor class */
typedef struct _NFFColor {
	float           r,
	                g,
	                b;
} NFFColor;

/* 
 * classes according to NFF format (for details, see SPD documentation)
 */

/* NFFViewpoint class */
typedef struct _NFFViewpoint {
	NFFPoint        from;
	NFFPoint        at;
	NFFPoint        up;
	float           angle;
	float           hither;
	float           aspect_ratio;
	int             xres,
	                yres;
} NFFViewpoint;

/* NFFBackgroundColor class */
typedef NFFColor NFFBackgroundColor;

/* NFFLight class */
typedef struct _NFFLight {
	NFFPoint        position;
	NFFColor        color;
} NFFLight;

/* NFFMaterial class */
typedef struct _NFFMaterial {
	NFFColor        color;
	float           Kd,
	                Ks,
	                Shine,
	                T,
	                index_of_refraction;
} NFFMaterial;

/* NFFCylCone class */
typedef struct _NFFCylCone {
	NFFPoint        base;
	float           base_radius;
	NFFPoint        apex;
	float           apex_radius;
} NFFCylCone;

/* NFFSphere class */
typedef struct _NFFSphere {
	NFFPoint        center;
	float           radius;
} NFFSphere;

/* NFFPolygon class */
typedef struct _NFFPolygon {
	int             numVertices;
	NFFPoint       *pVertices;
} NFFPolygon;

/* NFFPolygonalPatch class */
typedef struct _NFFPolygonalPatch {
	int             numVertices;
	NFFPoint       *pVertices;
	NFFPoint       *pNormals;
} NFFPolygonalPatch;

#endif
