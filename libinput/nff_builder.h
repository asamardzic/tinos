/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NFF_BUILDER_H
#define NFF_BUILDER_H

#include "nff.h"

/* NFFBuilder class */
typedef struct _NFFBuilder {
	void            (*PreBuild) (struct _NFFBuilder *);
	void            (*BuildBackgroundColor) (struct _NFFBuilder *,
						 NFFBackgroundColor *);
	void            (*BuildCylCone) (struct _NFFBuilder *,
					 NFFCylCone *);
	void            (*BuildLight) (struct _NFFBuilder *, NFFLight *);
	void            (*BuildMaterial) (struct _NFFBuilder *,
					  NFFMaterial *);
	void            (*BuildPolygon) (struct _NFFBuilder *,
					 NFFPolygon *);
	void            (*BuildPolygonalPatch) (struct _NFFBuilder *,
						NFFPolygonalPatch *);
	void            (*BuildSphere) (struct _NFFBuilder *, NFFSphere *);
	void            (*BuildViewpoint) (struct _NFFBuilder *,
					   NFFViewpoint *);
	void            (*PostBuild) (struct _NFFBuilder *);
} NFFBuilder;

/* NFFBuilder class declarators */
#define NFFBUILDER_DECLARE(nffBuilder) \
    PNFFBuilder nffBuilder = { \
    NFFBuilder_PreBuild, NFFBuilder_BackgroundColor, \
    NFFBuilder_CylCone, NFFBuilder_Light, \
    NFFBuilder_Material, NFFBuilder_Polygon, \
    NFFBuilder_PolygonalPatch, NFFBuilder_Sphere, \
    NFFBuilder_Viewpoint, NFFBuilder_PostBuild};

#define NFFBUILDER_DECLAREDYNAMIC(nffBuilder) \
    (nffBuilder).PreBuild = NFFBuilder_PreBuild, \
    (nffBuilder).BuildBackgroundColor = NFFBuilder_BackgroundColor, \
    (nffBuilder).BuildCylCone = NFFBuilder_CylCone, \
    (nffBuilder).BuildLight = NFFBuilder_Light, \
    (nffBuilder).BuildMaterial = NFFBuilder_Material, \
    (nffBuilder).BuildPolygon = NFFBuilder_Polygon, \
    (nffBuilder).BuildPatch = NFFBuilder_PolygonalPatch, \
    (nffBuilder).BuildSphere = NFFBuilder_Sphere, \
    (nffBuilder).BuildViewpoint = NFFBuilder_Viewpoint, \
    (nffBuilder).PostBuild = NFFBuilder_PostBuild;

/* NFFBuilder class virtual overrides */
void            NFFBuilder_PreBuild(NFFBuilder *);
void            NFFBuilder_BackgroundColor(NFFBuilder *,
					   NFFBackgroundColor *);
void            NFFBuilder_CylCone(NFFBuilder *, NFFCylCone *);
void            NFFBuilder_Light(NFFBuilder *, NFFLight *);
void            NFFBuilder_Material(NFFBuilder *, NFFMaterial *);
void            NFFBuilder_Polygon(NFFBuilder *, NFFPolygon *);
void            NFFBuilder_PolygonalPatch(NFFBuilder *,
					  NFFPolygonalPatch *);
void            NFFBuilder_Sphere(NFFBuilder *, NFFSphere *);
void            NFFBuilder_Viewpoint(NFFBuilder *, NFFViewpoint *);
void            NFFBuilder_PostBuild(NFFBuilder *);

#endif
