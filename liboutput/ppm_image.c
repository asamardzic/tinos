/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <assert.h>		/* for assert() function */
#include <stdlib.h>		/* for malloc() function */
#include <string.h>		/* for strlen() function */
#include "common.h"
#include "ppm_image.h"

/* PPMImage_Init - PPMImage class constructor */
void
PPMImage_Init(this, width, height, maxValue, comment)
     PPMImage       *this;
     int             width;
     int             height;
     int             maxValue;
     char           *comment;
{
	/* set pixmap size */
	this->width = width;
	this->height = height;

	/* remember maximum color value */
	this->maxValue = maxValue;

	/* allocate storage for pixels */
	this->pPixels =
	    malloc(3 * width * height *
		   ((maxValue <= 255) ? sizeof(Byte) : sizeof(Word)));
	assert(this->pPixels != NULL);

	/* remember comment */
	this->comment = malloc((strlen(comment) + 1) * sizeof(char));
	assert(this->comment != NULL);
	strcpy(this->comment, comment);
}

/* PPMImage_Clean - PPMImage class destructor */
void
PPMImage_Clean(this)
     PPMImage       *this;
{
	free(this->comment);
	free(this->pPixels);
}

/* PPMImage_Deserialize - write pixmap to standard output */
int
PPMImage_Deserialize(this)
     PPMImage       *this;
{
	/* write header */
	printf("P6\n");

	/* write image size */
	printf("%d %d\n", this->width, this->height);

	/* write comment */
	printf("# %s\n", this->comment);

	/* write color component maximum value */
	printf("%d\n", this->maxValue);

	/* write pixels */
	if (this->maxValue <= 255)
		fwrite(this->pPixels, 3 * sizeof(Byte),
		       this->width * this->height, stdout);
	else
		fwrite(this->pPixels, 3 * sizeof(Word),
		       this->width * this->height, stdout);

	return 1;
}
