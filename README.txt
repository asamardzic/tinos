WHAT IS IT?

Tinos is a multi-threaded shared memory Ray Tracing program.  Tinos
works as a regular UNIX filter accepting scene description in NFF format
on standard input and emitting image in PPM format to standard output
(SPD package could be used to generate input scenes in NFF format).


REQUIREMENTS

Tinos is using OpenMP API or POSIX threads for multi-threading.  If
OpenMP API used, OpenMP capable C compiler is required (for example, gcc
from version 4.2 onwards).  If POSIX threads used, appropriate libraries
should be installed on system.


INSTALLATION

Tinos is using cmake (http://www.cmake.org/) make system.  Thus,
provided that cmake installed on given machine, Tinos could be compiled
and installed trough following sequence of commands (executed in tinos
directory):
  cmake .
  make
  make install

By default, Tinos is configured to use OpenMP.  If use of pthreads API
preferred instead, then first command in above sequence should be
replaced by something like :
  cmake -DOPENMP:BOOL=FALSE .


DOCUMENTATION

After installation, issue command:
  man tinos
to access Tinos manpage.

For Tinos internals, as usual with open source projects, source code is
ultimate reference.
