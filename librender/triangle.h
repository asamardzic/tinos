/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "primitive.h"

/* Triangle class */
typedef struct _PTriangle {
	void            (*Clean) (PPrimitive *);
	                Bool(*Intersect) (struct _PTriangle *, PLine *);
	void            (*CalcNormal) (struct _PTriangle *, PVector,
				       PVector);
	void            (*CalcBoundingVolume) (struct _PTriangle *,
					       PBoundingVolume *,
					       PSlabs *);
	void            (*Voxelize) (struct _PTriangle *,
				     struct _PVoxelGrid *);
	PMaterial      *pMaterial;
	PBoundingVolume *pNode;
	DWord           rayID;
	PPlane          plane;
	PVertex         vertex[3];
	double          p,
	                q;
	PVector         P,
	                Q;
} PTriangle;

/* Triangle class declarators */
#define TRIANGLE_DECLARE(triangle) \
    PPrimitive    triangle= \
        {Primitive_Clean, \
        Triangle_Intersect, \
        Triangle_CalcNormal, \
        Triangle_CalcBoundingVolume, \
        Triangle_Voxelize};
#define TRIANGLE_DECLAREDYNAMIC(triangle) \
    (triangle).Clean=Primitive_Clean, \
    (triangle).Intersect=Triangle_Intersect, \
    (triangle).CalcNormal=Triangle_CalcNormal, \
    (triangle).CalcBoundingVolume=Triangle_CalcBoundingVolume, \
    (triangle).Voxelize=Triangle_Voxelize;

/* Triangle class overrides */
Bool            Triangle_Intersect(PTriangle *, PLine *);
void            Triangle_CalcNormal(PTriangle *, PVector, PVector);
void            Triangle_CalcBoundingVolume(PTriangle *, PBoundingVolume *,
					    PSlabs *);
void            Triangle_Voxelize(PTriangle *, struct _PVoxelGrid *);
void            Triangle_CalcCoeffs(PTriangle *);

#endif
