/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ray.h"

/* Ray_CreateShadowRay - create shadow ray from intersection point and
 * light position */
void
Ray_CreateShadowRay(this, point, lightPosition)
     PRay           *this;
     PVector         point;
     PVector         lightPosition;
{
	double          mul;

	/* initalize shadow ray's t value to distance of point from light */
	Vector_Sub(this->dp, lightPosition, point);
	Vector_Normalize(this->dp, mul, this->t);

	/* initialize rest of ray members */
	Vector_Assign(this->p0, point);
	this->depth = 0;
}

/* Ray_CreateReflectedRay - create reflected ray from ray specifed,
 * intersection point and primitive normal */
Bool
Ray_CreateReflectedRay(this, pRay, point, normal)
     PRay           *this;
     PRay           *pRay;
     PVector         point;
     PVector         normal;
{
	double         *dp = this->dp;
	double          mul,
	                den;
	PVector         Vp;

	if ((den = Vector_DotProduct(pRay->dp, normal)) >= 0)	/* no
								 * reflection 
								 */
		return FALSE;

	/* reflection ray direction: R=2N-Vp */
	Vector_Assign(dp, normal);
	Vector_MulAssign(dp, 2);
	Vector_Mul(Vp, pRay->dp, 1 / den);
	Vector_SubAssign(dp, Vp);
	Vector_Normalize(this->dp, mul, den);

	/* initialize rest of ray members */
	Vector_Assign(this->p0, point);
	this->depth = pRay->depth + 1;
	this->t = DBL_MAX;
	return TRUE;
}

/* Ray_CreateRefractedRay - create reflected ray from ray specifed,
 * intersection point, primitive normal and index od refraction */
Bool
Ray_CreateRefractedRay(this, pRay, point, normal, ir)
     PRay           *this;
     PRay           *pRay;
     PVector         point;
     PVector         normal;
     double          ir;
{
	PVector         Vp;
	double          kf;
	double          den;

	if ((den = Vector_DotProduct(pRay->dp, normal)) >= 0)	/* no
								 * refraction 
								 */
		return FALSE;

	/* refraction ray direction: T=kf*(N-Vp)-N */
	Vector_Assign(this->dp, normal);
	Vector_Mul(Vp, pRay->dp, 1 / den);
	Vector_SubAssign(this->dp, Vp);

	kf = ir * ir * (Vp[X] * Vp[X] + Vp[Y] * Vp[Y] + Vp[Z] * Vp[Z]) -
	    (this->dp[X] * this->dp[X] + this->dp[Y] * this->dp[Y] +
	     this->dp[Z] * this->dp[Z]);
	if (kf <= 0)		/* total internal reflection */
		return FALSE;
	kf = 1 / sqrt(kf);

	Vector_MulAssign(this->dp, kf);
	Vector_SubAssign(this->dp, normal);

	/* initialize rest of ray members */
	Vector_Assign(this->p0, point);
	this->depth = pRay->depth + 1;
	this->t = DBL_MAX;
	return TRUE;
}
