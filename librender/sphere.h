/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SPHERE_H
#define SPHERE_H

#include "primitive.h"

/* Sphere class */
typedef struct _PSphere {
	void            (*Clean) (struct _PPrimitive *);
	                Bool(*Intersect) (struct _PSphere *, PLine *);
	void            (*CalcNormal) (struct _PSphere *, PVector,
				       PVector);
	void            (*CalcBoundingVolume) (struct _PSphere *,
					       PBoundingVolume *,
					       PSlabs *);
	void            (*Voxelize) (struct _PSphere *,
				     struct _PVoxelGrid *);
	PMaterial      *pMaterial;
	PBoundingVolume *pNode;
	DWord           rayID;
	PVector         center;
	double          radius;
} PSphere;

/* Sphere class declarators */
#define SPHERE_DECLARE(sphere) \
    PPrimitive    sphere= \
        {Primitive_Clean, \
        Sphere_Intersect, \
        Sphere_CalcNormal, \
        Sphere_CalcBoundingVolume, \
        Sphere_Voxelize};
#define SPHERE_DECLAREDYNAMIC(sphere) \
    (sphere).Clean=Primitive_Clean, \
    (sphere).Intersect=Sphere_Intersect, \
    (sphere).CalcNormal=Sphere_CalcNormal, \
    (sphere).CalcBoundingVolume=Sphere_CalcBoundingVolume, \
    (sphere).Voxelize=Sphere_Voxelize;

/* Sphere class overrides */
Bool            Sphere_Intersect(PSphere *, PLine *);
void            Sphere_CalcNormal(PSphere *, PVector, PVector);
void            Sphere_CalcBoundingVolume(PSphere *, PBoundingVolume *,
					  PSlabs *);
void            Sphere_Voxelize(PSphere *, struct _PVoxelGrid *);

#endif
