/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCENE_BUILDER_H
#define SCENE_BUILDER_H

#include "input.h"
#include "scene.h"

/* SceneBuilder class - subclass of NFFBuilder */
typedef struct _PSceneBuilder {
	void            (*PreBuild) (NFFBuilder *);
	void            (*BuildBackgroundColor) (struct _PSceneBuilder *,
						 NFFBackgroundColor *);
	void            (*BuildCylCone) (NFFBuilder *, NFFCylCone *);
	void            (*BuildLight) (struct _PSceneBuilder *,
				       NFFLight *);
	void            (*BuildMaterial) (struct _PSceneBuilder *,
					  NFFMaterial *);
	void            (*BuildPolygon) (struct _PSceneBuilder *,
					 NFFPolygon *);
	void            (*BuildPolygonalPatch) (struct _PSceneBuilder *,
						NFFPolygonalPatch *);
	void            (*BuildSphere) (struct _PSceneBuilder *,
					NFFSphere *);
	void            (*BuildViewpoint) (struct _PSceneBuilder *,
					   NFFViewpoint *);
	void            (*PostBuild) (struct _PSceneBuilder *);
	PScene         *pScene;
	PMaterial      *pCurrMaterial;
	Bool            viewpointBuilt;
} PSceneBuilder;

/* SceneBuilder class declarators */
#define SCENEBUILDER_DECLARE(sceneBuilder) \
    PSceneBuilder    sceneBuilder= \
        {NFFBuilder_PreBuild, \
        SceneBuilder_BackgroundColor, \
        SceneBuilder_CylCone, \
        SceneBuilder_Light, \
        SceneBuilder_Material, \
        SceneBuilder_Polygon, \
        SceneBuilder_PolygonalPatch, \
        SceneBuilder_Sphere, \
        SceneBuilder_Viewpoint, \
        SceneBuilder_PostBuild};
#define SCENEBUILDER_DECLAREDYNAMIC(sceneBuilder) \
    (sceneBuilder).PreBuild=NFFBuilder_PreBuild, \
    (sceneBuilder).BuildBackgroundColor=SceneBuilder_BackgroundColor, \
    (sceneBuilder).BuildCylCone=SceneBuilder_CylCone, \
    (sceneBuilder).BuildLight=SceneBuilder_Light, \
    (sceneBuilder).BuildMaterial=SceneBuilder_Material, \
    (sceneBuilder).BuildPolygon=SceneBuilder_Polygon, \
    (sceneBuilder).BuildPolygonalPatch=SceneBuilder_PolygonalPatch, \
    (sceneBuilder).BuildSphere=SceneBuilder_Sphere, \
    (sceneBuilder).BuildViewpoint=SceneBuilder_Viewpoint, \
    (sceneBuilder).PostBuild=SceneBuilder_PostBuild};

/* SceneBuilder public operations */
void            SceneBuilder_Init(PSceneBuilder *, PScene *);

/* SceneBuilder class virtual overrides */
void            SceneBuilder_BackgroundColor(PSceneBuilder *,
					     NFFBackgroundColor *);
void            SceneBuilder_CylCone(PSceneBuilder *, NFFCylCone *);
void            SceneBuilder_Light(PSceneBuilder *, NFFLight *);
void            SceneBuilder_Material(PSceneBuilder *, NFFMaterial *);
void            SceneBuilder_Polygon(PSceneBuilder *, NFFPolygon *);
void            SceneBuilder_PolygonalPatch(PSceneBuilder *,
					    NFFPolygonalPatch *);
void            SceneBuilder_Sphere(PSceneBuilder *, NFFSphere *);
void            SceneBuilder_Viewpoint(PSceneBuilder *, NFFViewpoint *);
void            SceneBuilder_PostBuild(PSceneBuilder *);


#endif
