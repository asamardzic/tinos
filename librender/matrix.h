/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MATRIX_H
#define MATRIX_H

#include <string.h>		/* for memcpy() function */
#include "common.h"

/* Matrix class */
typedef double  PMatrix[16];

/* Matrix class public operations */
#define Matrix_Assign(A,B) \
    memcpy(A,B,sizeof(PMatrix));
#define Matrix_LoadIdentity(A) \
    { \
        (A)[0]=1, (A)[1]=0, (A)[2]=0, (A)[3]=0; \
        (A)[4]=0, (A)[5]=1, (A)[6]=0, (A)[7]=0; \
        (A)[8]=0, (A)[9]=0, (A)[10]=1, (A)[11]=0; \
        (A)[12]=0, (A)[13]=0, (A)[14]=0, (A)[15]=1; \
    }
#define Matrix_MulVector(A,M,B) \
    { \
        (A)[X]=(M)[0]*(B)[X]+(M)[1]*(B)[Y]+(M)[2]*(B)[Z]+(M)[3]; \
        (A)[Y]=(M)[4]*(B)[X]+(M)[5]*(B)[Y]+(M)[6]*(B)[Z]+(M)[7]; \
        (A)[Z]=(M)[8]*(B)[X]+(M)[9]*(B)[Y]+(M)[10]*(B)[Z]+(M)[11]; \
    }

void            Matrix_SwapRows(PMatrix, int, int);
Bool            Matrix_Invert(PMatrix, PMatrix);

#endif
