/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef VECTOR_H
#define VECTOR_H

#include <math.h>		/* for sqrt() function */
#include <string.h>		/* for memcpy() function */

/* vector component indices */
#define X           0
#define Y           1
#define Z           2
#define W           3

/* Vector class */
typedef double  PVector[3];

/* Vector class public operations */
#define Vector_Assign(A,B) \
    memcpy(A,B,sizeof(PVector));

#define Vector_Norm(A) \
    (sqrt((A)[X]*(A)[X]+(A)[Y]*(A)[Y]+(A)[Z]*(A)[Z]))

#define Vector_Normalize(A,mul,den) \
    if (den=Vector_Norm(A)) \
    { \
        mul=1/den; \
        (A)[X]*=mul; \
        (A)[Y]*=mul; \
        (A)[Z]*=mul; \
    }

#define Vector_AddAssign(A,B) \
    { \
        (A)[X]+=(B)[X]; \
        (A)[Y]+=(B)[Y]; \
        (A)[Z]+=(B)[Z]; \
    }

#define Vector_Sub(A,B,C) \
    { \
        (A)[X]=(B)[X]-(C)[X]; \
        (A)[Y]=(B)[Y]-(C)[Y]; \
        (A)[Z]=(B)[Z]-(C)[Z]; \
    }

#define Vector_SubAssign(A,B) \
    { \
        (A)[X]-=(B)[X]; \
        (A)[Y]-=(B)[Y]; \
        (A)[Z]-=(B)[Z]; \
    }

#define Vector_MulAssign(A,t) \
    { \
        (A)[X]*=(t); \
        (A)[Y]*=(t); \
        (A)[Z]*=(t); \
    }

#define Vector_Mul(A,B,t) \
    { \
        (A)[X]=(B)[X]*(t); \
        (A)[Y]=(B)[Y]*(t); \
        (A)[Z]=(B)[Z]*(t); \
    }

#define Vector_DotProduct(A,B) \
    ((A)[X]*(B)[X]+(A)[Y]*(B)[Y]+(A)[Z]*(B)[Z])

#define Vector_CrossProduct(A,B,C) \
    { \
        (A)[X]=(B)[Y]*(C)[Z]-(B)[Z]*(C)[Y]; \
        (A)[Y]=(B)[Z]*(C)[X]-(B)[X]*(C)[Z]; \
        (A)[Z]=(B)[X]*(C)[Y]-(B)[Y]*(C)[X]; \
    }

#define Vector_Invert(A,B) \
    { \
        (A)[X]=-(B)[X]; \
        (A)[Y]=-(B)[Y]; \
        (A)[Z]=-(B)[Z]; \
    }

#define Vector_Halfway(A,B,C) \
    { \
        (A)[X]=((B)[X]+(C)[X])/2; \
        (A)[Y]=((B)[Y]+(C)[Y])/2; \
        (A)[Z]=((B)[Z]+(C)[Z])/2; \
    }

#define Vector_LERP(A,B,C,t) \
    { \
        (A)[X]=(B)[X]+t*(C)[X]; \
        (A)[Y]=(B)[Y]+t*(C)[Y]; \
        (A)[Z]=(B)[Z]+t*(C)[Z]; \
    }

#endif
