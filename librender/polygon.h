/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef POLYGON_H
#define POLYGON_H

#include "primitive.h"

typedef struct _PPolygon {
	void            (*Clean) (struct _PPolygon *);
	                Bool(*Intersect) (struct _PPolygon *, PLine *);
	void            (*CalcNormal) (struct _PPolygon *, PVector,
				       PVector);
	void            (*CalcBoundingVolume) (struct _PPolygon *,
					       PBoundingVolume *,
					       PSlabs *);
	void            (*Voxelize) (struct _PPolygon *,
				     struct _PVoxelGrid *);
	PMaterial      *pMaterial;
	PBoundingVolume *pNode;
	DWord           rayID;
	DWord           numVertices;
	PVertex        *vertex;
	PPlane          plane;
} PPolygon;

/* Polygon class declarators */
#define POLYGON_DECLARE(polygon) \
    PPrimitive    polygon= \
        {Polygon_Clean, \
        Polygon_Intersect, \
        Polygon_CalcNormal, \
        Polygon_CalcBoundingVolume, \
        Polygon_Voxelize};
#define POLYGON_DECLAREDYNAMIC(polygon) \
    (polygon).Clean=Polygon_Clean, \
    (polygon).Intersect=Polygon_Intersect, \
    (polygon).CalcNormal=Polygon_CalcNormal, \
    (polygon).CalcBoundingVolume=Polygon_CalcBoundingVolume, \
    (polygon).Voxelize=Polygon_Voxelize;

/* Polygon class overrides */
void            Polygon_Clean(PPolygon *);
Bool            Polygon_Intersect(PPolygon *, PLine *);
void            Polygon_CalcNormal(PPolygon *, PVector, PVector);
void            Polygon_CalcBoundingVolume(PPolygon *, PBoundingVolume *,
					   PSlabs *);
void            Polygon_Voxelize(PPolygon *, struct _PVoxelGrid *);
void            Polygon_Init(PPolygon *, DWord);

#endif
