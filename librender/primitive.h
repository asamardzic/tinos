/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PRIMITIVE_H
#define PRIMITIVE_H

#include "common.h"
#include "bounding_volume.h"
#include "line.h"
#include "material.h"
#include "plane.h"
#include "primitive.h"
#include "slabs.h"
#include "vertex.h"

struct _PVoxelGrid;

/* Primitive class */
typedef struct _PPrimitive {
	void            (*Clean) (struct _PPrimitive *);
	                Bool(*Intersect) (struct _PPrimitive *, PLine *);
	void            (*CalcNormal) (struct _PPrimitive *, PVector,
				       PVector);
	void            (*CalcBoundingVolume) (struct _PPrimitive *,
					       PBoundingVolume *,
					       PSlabs *);
	void            (*Voxelize) (struct _PPrimitive *,
				     struct _PVoxelGrid *);
	PMaterial      *pMaterial;
	PBoundingVolume *pBoundingVolume;
	DWord           rayID;
} PPrimitive;

/* Primitive class declarators */
#define PRIMITIVE_DECLARE(primitive) \
    PPrimitive    primitive= \
        {Primitive_Clean, \
        NULL, \
        NULL, \
        NULL, \
        NULL};
#define PRIMITIVE_DECLAREDYNAMIC(primitive) \
    (primitive).Clean=Primitive_Clean, \
    (primitive).CalcNormal=NULL, \
    (primitive).Intersect=NULL, \
    (primitive).CalcBoundingVolume=NULL, \
    (primitive).Voxelize=NULL;

/* Primitive class public operations */
void            Primitive_Clean(PPrimitive *);

#endif
