/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BOUNDING_VOLUME_HIERARCHY_H
#define BOUNDING_VOLUME_HIERARCHY_H

#include "common.h"
#include "data_access_structure.h"

/* BoundingVolumeHierarchy class */
typedef struct _PBoundingVolumeHierarchy {
	void            (*Clean) (struct _PBoundingVolumeHierarchy *);
	PPrimitive     *(*Intersect) (struct _PBoundingVolumeHierarchy *,
				      PRay *);
	PSlabs          slabs;
	DWord           numPrimitives;
	PTree           tree;
} PBoundingVolumeHierarchy;

/* BoundingVolumeHierarchy class declarators */
#define BOUNDINGVOLUMEHIERARCHY_DECLARE(boundingVolumeHierarchy) \
    PBoundingVolumeHierarchy    boundingVolumeHierarchy= \
        {BoundingVolumeHierarchy_Clean, \
        BoundingVolumeHierarchy_Intersect};
#define BOUNDINGVOLUMEHIERARCHY_DECLAREDYNAMIC(boundingVolumeHierarchy) \
    (boundingVolumeHierarchy).Clean=BoundingVolumeHierarchy_Clean, \
    (boundingVolumeHierarchy).Intersect=BoundingVolumeHierarchy_Intersect;

/* BoundingVolumeHierarchy class public operations */
void            BoundingVolumeHierarchy_Init(PBoundingVolumeHierarchy *,
					     PSlabs *, PList *, int);

/* BoundingVolumeHierarchy class virtual overrides */
void            BoundingVolumeHierarchy_Clean(PBoundingVolumeHierarchy *);
PPrimitive     *BoundingVolumeHierarchy_Intersect(PBoundingVolumeHierarchy
						  *, PRay *);

#endif
