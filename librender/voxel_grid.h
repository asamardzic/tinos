/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef VOXEL_GRID_H
#define VOXEL_GRID_H

#include "common.h"
#include "data_access_structure.h"

/* Voxel Grid class */
typedef struct _PVoxelGrid {
	void            (*Clean) (struct _PVoxelGrid *);
	PPrimitive     *(*Intersect) (struct _PVoxelGrid *, PRay *);
	PArray          cells;
	double          d[3][2];
	DWord           numCells[3];
	double          hCell;
} PVoxelGrid;

/* Voxel Grid class declarators */
#define VOXELGRID_DECLARE(voxelGrid) \
    PVoxelGrid    voxelGrid= \
        {VoxelGrid_Clean, \
        VoxelGrid_Intersect};
#define VOXELGRID_DECLAREDYNAMIC(voxelGrid) \
    (voxelGrid).Clean=VoxelGrid_Clean, \
    (voxelGrid).Intersect=VoxelGrid_Intersect;

/* Voxel Grid class public operations */
void            VoxelGrid_Init(PVoxelGrid *, PList *, PSlabs *);
void            VoxelGrid_Insert(PVoxelGrid *, DWord *, void *);
void            VoxelGrid_EnumerateCellsLine(PVoxelGrid *, double *,
					     double *, double *,
					     void (PVoxelGrid *, DWord *,
						   void *), void *);
void            VoxelGrid_EnumerateCellsCircleXY(PVoxelGrid *, double *,
						 double, double, double *,
						 void (PVoxelGrid *,
						       DWord *, void *),
						 void *);
void            VoxelGrid_CellIntersect(PVoxelGrid *, DWord *, void *);

/* Voxel Grid class virtual overrides */
void            VoxelGrid_Clean(PVoxelGrid *);
PPrimitive     *VoxelGrid_Intersect(PVoxelGrid *, PRay *);

#endif
