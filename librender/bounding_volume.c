/*
 * Copyright (c) 1997, 1998, 2003, 2006 Aleksandar Samardzic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>		/* for assert() function */
#include <stdlib.h>		/* for malloc() function */
#include "bounding_volume.h"
#include "slabs.h"

/* BoundingVolume_Init - BoundingVolume class constructor */
void
BoundingVolume_Init(this, pSlabs)
     PBoundingVolume *this;
     PSlabs         *pSlabs;
{
	this->numSlabs = pSlabs->numSlabs;
	this->d = malloc(this->numSlabs * sizeof(double[2]));
	assert(this->d != NULL);
	this->pPrimitive = NULL;
}

/* BoundingVolume_Clean - BoundingVolume class destructor */
void
BoundingVolume_Clean(this)
     PBoundingVolume *this;
{
	free(this->d);
}

/* BoundingVolume_Intersect - intersecting bounding volume with a ray
 * according to [Kay86] */
double
BoundingVolume_Intersect(this, S, T)
     PBoundingVolume *this;
     double         *S;
     double         *T;
{
	Byte            slab,
	                numSlabs = this->numSlabs;
	double          (*d)[2] = this->d;
	double          t_node[2],
	                t_slab[2];
	Byte            lo,
	                hi;

	/* initialize bounding volume intersections */
	t_node[LO] = -DBL_MAX, t_node[HI] = DBL_MAX;

	for (slab = 0; slab < numSlabs; slab++)	/* for each slab */
		if (T[slab] != DBL_MAX) {	/* ray is not parallel
						 * with slabs */
			/* find ray intersections with slab */
			t_slab[0] = (d[slab][LO] - S[slab]) * T[slab];
			t_slab[1] = (d[slab][HI] - S[slab]) * T[slab];

			/* determine closer and farther intersection with
			 * current slab */
			if (t_slab[0] < t_slab[1])
				lo = 0, hi = 1;
			else
				lo = 1, hi = 0;

			/* compare slab intersections with bounding volume 
			 * intersecions */
			if (t_slab[lo] > t_node[LO])
				t_node[LO] = t_slab[lo];
			if (t_slab[hi] < t_node[HI])
				t_node[HI] = t_slab[hi];

			if (t_node[HI] < t_node[LO])	/* ray misses
							 * bounding volume 
							 */
				return DBL_MAX;
		} else if (S[slab] - d[slab][LO] < 0 || S[slab] - d[slab][HI] > 0)	/* ray 
											 * misses 
											 * bounding 
											 * volume 
											 */
			return DBL_MAX;

	return t_node[LO];
}

/* BoundingVolume_Combine - combine two bounding volumes */
void
BoundingVolume_Combine(this, first, second)
     PBoundingVolume *this;
     PBoundingVolume *first;
     PBoundingVolume *second;
{
	Byte            slab,
	                numSlabs = this->numSlabs;

	for (slab = 0; slab < numSlabs; slab++) {
		this->d[slab][LO] =
		    Min(first->d[slab][LO], second->d[slab][LO]);
		this->d[slab][HI] =
		    Max(first->d[slab][HI], second->d[slab][HI]);
	}
}

/* BoundingVolume_CalcArea - calculate bounding volume area */
/* NOTE: this function is restricted to prismatic bounding volumes */
void
BoundingVolume_CalcArea(this)
     PBoundingVolume *this;
{
	Byte            slab,
	                numSlabs = this->numSlabs;
	double         *h;

	/* find width of each slab */
	h = malloc(numSlabs * sizeof(double));
	assert(h != NULL);
	for (slab = 0; slab < numSlabs; slab++)
		h[slab] = this->d[slab][HI] - this->d[slab][LO];

	/* calculate area */
	this->area = 0;
	for (slab = 0; slab < numSlabs; slab++)
		this->area += h[slab] * h[(slab + 1) % numSlabs];
	free(h);
}
